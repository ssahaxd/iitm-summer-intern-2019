# Abstract

Video is one of the most consumed media over the web. Popular video streaming sites like YouTube and Netflix uses different strategy to deliver the video over the web to there users depending on the platform (i.e. PC or Mobile device) on the user is watching the video and the container on which the video is playing (Adobe flash, Microsoft Silverlight, HTML5).

In a typical video stream there are two phases, in the first phase called buffering a chunk of the video file is downloaded in to the users device and the video playback stats with out waiting for the buffering to end, the amount of data downloaded, depends on the platform and the container. The second phase is called the steady state phase in this phase periodically video data is transferred to the users device. Depending on the period of the data transfer in the steady state phase there can be three possibilities, No ON-OFF cycles short ON-OFF cycles or Long ON-OFF cycles.

In case No ON-OFF cycle the whole video data is transferred to the user in the buffering phase. where short and long ON-OFF cycle has short pause and small data chunk and long pause and big data chunk between two consecutive transfers respectively.



# YouTube Streaming Strategies

- In the buffering stage around 40 sec worth of data is downloaded
- In steady state 64kB data is periodically transferred  
- Short ON-OFF cycle for flash videos and HTML5 videos streamed in IE
- Long ON-OFF cycle may be observed if there is a significant packet loss.
- Long ON-OFF cycles for HTML5 and mobile videos
- iPad uses more than one strategy to stream YouTube videos
- No ON-OFF cycles for html5 in Firefox and flash HD videos

streaming strategy for YouTube and Netflix depends on the application and the container, data transfer rate can be controlled by the server or by the application, depending on the application and the container different strategy are used.

NO ON-OFF cycle is same as file transfer over tcp, the bandwidth my be wasted due to lack of interest of the user. Short  and LONG ON-OFF cycle require much complex design and can greatly save some bandwidth.

# Key problems of the paper

- In this paper we see the authors uses flash and Adobe Flash, Microsoft Silverlight along with HTML5 as a video container, but Adobe Flash and Microsoft Silverlight has been deprecated and mostly HTML5 player is used.
- The highest resolution supported at the time of this paper is written is 720p but now we streams videos which may be 4k in resolution.
- Authors considered YouTube video playback in HTML5 at default resolution i.e. 360p

# Suggestion

During this seven, eight years many things have been changed, Adobe Flash and Microsoft Silverlight is deprecated, the whole web is sifted to HTML5, IE is replaced with Microsoft edge.

Mobile phone are much powerful then ever been, During this years YouTube and Netflix evolved  to a great extent. Thus I feel the streaming strategy of these sites are also changed.  

