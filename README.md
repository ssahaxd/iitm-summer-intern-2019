- When ever a choice is selected in the movie the amount of get request for the upcoming video segments increases suddenly.

- So to determine when a choice point is encountered we can visually detect a heavy traffic in the traffic vs time graph.

- As soon as the choice appear, we have 10 sec to click on any given option and when the choice  is selected the traffic rate will go high. 

- So the choice point will be with in 10 sec before the traffic rate goes high.

- First filter out all unnecessary packets by the IP address of the client and the Netflix server.

- To find the peak we can create 2 arrays one to keep track of the `time` and other `packet_count` to keep track of the number of packets at `i th` second.

- Then for each packet in the trace file
  * if the packet is captured at `i th` second then increase the value of the `packet_count[i]` by `one` 

```python
i = 0
pkt_cnt = [] 
time = []
start_time = floor(packets[0].Time)
for packet in packets:
    curr_time = floor(packet.Time)
    if(curr_time > start_time):
        i++
        pkt_cnt.append(1)
        time.append(curr_time)
    else:
        pkt_cnt[i]++
        
```

- Now we have the number of packets in each second. so we can start working to find the when the packet rate is high.

- For finding the peak we take the difference between every consecutive pair in the `packet_count` array .

- If the the difference is greater than some threshold value (say 200) then we mark it as the start of the peak.

- If the difference is smaller than some threshold value (say -200) then we mark it as end of the peak.

- At the end, return a list of all possible pairs of start peak time and end peak time.

```python
delta = 0
peaks = []
for i in range(1, len(pkt_cnt)):
    delta = pkt_cnt[i] - pkt_cnt[i-1]
    if delta > someThreshold:
        startTime = time [i]
    elif delta < someThreshold:
        endTime = time[i]
        peaks.append([startTime, endTime])
```

---

#### Algorithm to distinguish between the default and non default selections

Step1:  Find the time when the movie starts

Step2:  Extract the cl2 JSON files by applying the filter: 5400 <= ssl.record.length <= 6800

// After Step2 the most of the captured packets are cl2 JSONs.

// Other than the JSON files some token-data files will be captured.

Step3:  As token-data files come in some specified places in the total captured packets, they can be removed
        by their relative time of arrival.

// After Step3 the non-JSON files will be removed to some significant extent

Step4:  List up all the subsequent files (cl2 JSON files) in Filtered_Packets list

Step5:  Initialize skip_itr = 0

    Label1: Iterate packet from Filtered_Packets:
    
            if skip_itr is greater than 0, then
                do
                decrement skip_itr by 1
                back to Label1
    
            Assign relative time value of the current packet into current_packet_time
    
            capture the next_packet for which:
            relative time of next_packet is in the range between (current_packet_time + 10) to (current_packet_time + 15)
    
            if ssl record length of next_packet - ssl record length of current packet is greater than 500, then
                do,
                Mouse clicked
    
                // skip one packet
                set the value of skip_itr to 1
    
            Otherwise
                do,
                Auto clicked
    
                // For non default choice selection, a type2 json is captured followed by a type1 json.
    
                // The ssl record lengths of the sever-site json files do not follow a specific pattern (found by observations).
    
                // Ssl record lengths of type1 json files for both default and non default choices are similar.
    
                // Only type2 json files are useful to differentiate between the default and non default choices.
    
                // The ssl record length of type2 json files are between 627 and 628.
    
                // In this range some other non-json files can be present.
    
                // So to distinguish the non default selections from the default ones we need to find if any packet
                  of length 627 or 628 is captured within 10 to 15 seconds of the current cl2 json files.
    
                // By doing so the non-json files can be eliminated to some significant extent.
    
                capture the next_type2_Json_packet for which:
                relative time of next_type2_Json_packet is in the range between (current_packet_time + 10) to (current_packet_time + 15)
    
                if ssl record length of next_type2_Json_packet is in the range between 627 to 628, then
                    do,
                    Non default choice is selected
    
                Otherwise
                    do,
                    Default choice is selected


                // skip two packets
                set the value of skip_itr to 2