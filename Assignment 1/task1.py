# !/usr/bin/env python

import pyshark

override_prefs = {
    'ssl.keylog_file': './sslkeylog.log'
}

filtered_cap = pyshark.FileCapture('dump.pcapng', only_summaries=False,
                                   display_filter='ssl.record.content_type==23 && ip.addr == 103.68.221.190 && http',
                                   override_prefs=override_prefs)
print("Please Wait Loading Packets...\n")
filtered_cap.load_packets()
number_of_packets = len(filtered_cap)

global req_frame_no, req, req_nxt_seq, res_frame_no, res, res_size, res_ack

# TODO: o / p: frame_info.number, tls.app_data, tcp.ack

# Version 1
# for i in range(number_of_packets):
#     chat = str(filtered_cap[i].http.chat)
#     index_of_http = chat.index("HTTP")
#     if index_of_http > 0:
#         req_frame_no = filtered_cap[i].frame_info.number
#         req = chat[:index_of_http - 1]
#         req_nxt_seq = int(filtered_cap[i].tcp.nxtseq)
#         print("req_obj: {}\t frame_no: {}\n".format(req, req_frame_no, ))
#         for j in range(i + 1, number_of_packets):
#             chat = str(filtered_cap[j].http.chat)
#             index_of_http = chat.index("HTTP")
#             if index_of_http == 0:
#                 res_ack = int(filtered_cap[j].tcp.ack)
#                 if res_ack == req_nxt_seq:
#                     res_frame_no = filtered_cap[j].frame_info.number
#                     res_size = filtered_cap[j].tls.record_length
#                     print("res_frame: {}\t res_size: {}\n\n".format(res_frame_no, res_size))
#                     break

# Version 2
output_file = open(r"./output.log", "w")

for i in range(number_of_packets):
    chat = str(filtered_cap[i].http.chat)
    chat = chat.split(' ')
    if chat[0] == 'GET':
        req_frame_no = filtered_cap[i].frame_info.number
        req = chat[1]
        req_nxt_seq = int(filtered_cap[i].tcp.nxtseq)
        str1 = "REQUEST: req_obj: {}\t frame_no: {}".format(req, req_frame_no)
        print(str1)
        output_file.write(str1)
        for j in range(i + 1, number_of_packets):
            chat = str(filtered_cap[j].http.chat)
            chat = chat.split(' ')
            if chat[0] == 'HTTP/1.1':
                res_ack = int(filtered_cap[j].tcp.ack)
                if res_ack == req_nxt_seq:
                    res_frame_no = filtered_cap[j].frame_info.number
                    res_size = filtered_cap[j].tls.record_length
                    str1 = "RESPONSE: res_frame: {}\t res_size: {}\n\n".format(res_frame_no, res_size)
                    print(str1)
                    output_file.write('\n' + str1)
